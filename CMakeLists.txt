
tdaq_package()

tdaq_add_library(MuCalDecode 
  src/CalibCheck.cxx 
  src/DataBuffer.cxx 
  src/CalibDataLoader.cxx 
  src/CalibData.cxx 
  src/CalibEvent.cxx 
  src/CalibFragment.cxx 
  src/CalibUti.cxx
  OPTIONS PRIVATE -Wno-strict-aliasing
  LINK_LIBRARIES DataReader)

tdaq_add_executable(test_mucal_dataloader 
  src/test/test_mucal_dataloader.cxx
  LINK_LIBRARIES MuCalDecode circ_proc)

tdaq_add_executable(test_mucal_circdump
  src/test/test_mucal_circdump.cxx
  LINK_LIBRARIES MuCalDecode circ_proc pthread)

