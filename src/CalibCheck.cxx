
#include <iostream>
#include <string>
#include <sstream>
#include <stdint.h>

#include "MuCalDecode/CalibEvent.h"
#include "MuCalDecode/DataBuffer.h"

namespace LVL2_MUON_CALIBRATION {

std::string iprint( uint32_t m_ip)
 {
  uint32_t a, b, c, d;
  a = (m_ip >> 24 & 0xff);
  b = (m_ip >> 16 & 0xff);
  c = (m_ip >> 8 & 0xff);
  d = (m_ip & 0xff);
  std::stringstream ss;
  ss << a << "." << b << "." << c << "." << d;
  return ss.str();
 }

std::string tprint( time_t & m_ttp)
 {
  char cbf[26];
  ctime_r(&m_ttp,cbf);
  for (int i=0; i<26; i++) if (cbf[i]==10) cbf[i]=0;
  return std::string(cbf);
 }

int CalibCheckA ( void * p, uint32_t size)
 {
  uint16_t * ptr = (uint16_t *)p;
  uint32_t fsz = ptr[2];
  if ((size) && (fsz != size))
   {
     return 1;
   }
  fsz >>= 1;
  uint16_t mask = ptr[0];
  if ((mask != ptr[fsz-1]) || (ptr[1] != EVTTAG) || (ptr[fsz-2] != EVTTAG)) {
    return 2;
  }
  if ((mask & 0xeeee) || !(mask)) {
    return 3;
  }

  return 0;
 }

int CalibCheckB ( void * p, uint32_t size)
 {
  if (size == 0) size = ((uint16_t*)p)[2];
  DataBuffer dBuff((uint8_t*)p, size);
  CalibEvent myCalibEvt(dBuff);
  return myCalibEvt.isValid() ? 0 : 4;
 }

int CalibCheck ( void * p, uint32_t size)
 {
  int ret1 = CalibCheckA ( p, size);
  int ret2 = (ret1) ? 0 : CalibCheckB ( p, size);
  return ret1 + ret2;
 }

} // end of namespace LVL2_MUON_CALIBRATION
