#include <cstring>
#include "MuCalDecode/CalibDataLoader.h"
#include "MuCalDecode/CalibData.h"
#include "MuCalDecode/CalibUti.h"

#include "EventStorage/pickDataReader.h"


using namespace LVL2_MUON_CALIBRATION;



CalibDataLoader::CalibDataLoader() : m_file(0),
                                     m_filename(""),
                                     m_debug(false),
				     m_type(DataBuffer::LITTLE_ENDIAN_WORD),
				     m_buffer_length(0),
                                     m_start_of_current_event(0),
				     m_end_of_current_event(0),
				     m_tot_bytes_read(0),
				     m_tot_bytes_processed(0),
				     m_max_event_to_process(0Xffffffff),
				     m_events_to_skip(0),
                                     m_events_read(0),
				     m_events_processed(0),
                                     m_events_corrupted(0),
				     m_bytes_skipped(0),
				     m_bad_event_flag(false),
				     m_buffer_size(DEFAULT_BUFFER_SIZE)  {
    
    m_buffer  = new uint8_t[m_buffer_size];
    m_skipped = new uint8_t[m_buffer_size];
    
    memset(m_buffer,0,m_buffer_size);
    memset(m_skipped,0,m_buffer_size);
}


CalibDataLoader::CalibDataLoader(std::string filename, bool debug,
    uint32_t events, uint32_t toSkip,uint32_t size, DataBuffer::WordType type ) 
                                   : m_file(0),
                                     m_filename(filename),
                                     m_debug(debug),
				     m_type(type),
				     m_buffer_length(0),
                                     m_start_of_current_event(0),
                                     m_end_of_current_event(0),
				     m_tot_bytes_read(0),
				     m_tot_bytes_processed(0),
				     m_max_event_to_process(events),
				     m_events_to_skip(toSkip),
                                     m_events_read(0),
				     m_events_processed(0),
                                     m_events_corrupted(0),
				     m_bytes_skipped(0),
				     m_bad_event_flag(false),
				     m_buffer_size(size)  {
    
    m_buffer  = new uint8_t[m_buffer_size];
    m_skipped = new uint8_t[m_buffer_size];
    
    memset(m_skipped,0,m_buffer_size);
    m_file = connect_file(m_filename);
    if(m_file) {
      m_tot_bytes_read += this->read_data( (char*)m_buffer, m_buffer_size );
      m_buffer_length   = m_tot_bytes_read;
    }
}

CalibDataLoader::~CalibDataLoader() {
    
    delete [] m_buffer;
    delete [] m_skipped;
}


fRead*
CalibDataLoader::connect_file(std::string filename) {

 
    fRead* datafile = loadfRead("fReadPlain");
    if(datafile==0) 
    {
        std::cout << "fReadPlain plugin not found; check the build!" 
	          << std::endl;
        return 0;
    }
    
    if( datafile->fileExists(filename) )
    {
        datafile->openFile(filename);
        if(! datafile->isOpen() ) 
        {
            // err message, delete the datafile and return 0
	    std::cout << "File \"" << filename << "\" not opened." << std::endl;
            delete datafile;
	    return 0;
        }
    }
    else
    {
        // try with castor
	delete datafile;
	datafile = loadfRead("fReadCastor");
	if(datafile==0) 
	{
	    std::cout << "fReadCastor plugin not found; check the build!" 
	          << std::endl;
	    std::cout << "File \"" << filename << "\" not found." << std::endl;
	    return 0;
	}
	
	if( datafile->fileExists(filename) )
        {
	    datafile->openFile(filename);
	    if(! datafile->isOpen() ) 
            {
                std::cout << "File \"" << filename << "\" not opened." << std::endl;
                delete datafile;
	        return 0;
            }
	} 
	else
	{
	    std::cout << "File \"" << filename << "\" not found." << std::endl;
            delete datafile;
	    return 0;
	} 
    }
    return datafile;
}


uint32_t          
CalibDataLoader::read_next_data(uint32_t start_pos) {

    if(m_file==0) return 0;
        
    if( !m_file->isEoF() ) {
	uint32_t size_to_shift = m_buffer_size - start_pos;
	uint32_t offset        = start_pos;
	
	uint32_t size_to_load  = m_buffer_size - size_to_shift;
		
	if (size_to_load == 0 ) return 0;
		
	//shift the bytes
	if(size_to_shift) memcpy(m_buffer,m_buffer+start_pos,size_to_shift);
	
	//load the data
        memset(m_buffer+size_to_shift, 0 ,size_to_load);
	
	uint32_t b = 
	      this->read_data((char*)(m_buffer+size_to_shift),size_to_load);
		
	m_buffer_length  += b;
	m_tot_bytes_read += b;
		
	//reset pointers
	m_end_of_current_event -= offset;
	m_start_of_current_event = (m_start_of_current_event>offset)?
	                            m_start_of_current_event - offset : 0;
	
	return offset;
    }
    
    return 0;
}

uint32_t
CalibDataLoader::find_next(void) {
    
    // set ot zero the skipped bytes
    memset(m_skipped,0,m_buffer_size);
    m_bytes_skipped = 0;
    
    uint32_t position = m_end_of_current_event;

    if (m_buffer_length==0) {
       position -= read_next_data(m_end_of_current_event); 
    }

    while (m_buffer_length>0) {
	
	// stop is event limit is reached
	if(m_events_read >= m_max_event_to_process) return m_buffer_size;
	
	//try to load more data if needed
	if( m_buffer_length < 8 ) {
	    position -= read_next_data(m_end_of_current_event);
	}
		
	uint16_t tag = 0x0;
	uint16_t con = 0x0;
        uint16_t version  = 0x0;
	uint16_t datasize = 0x0;
	
	if( m_buffer_length >= 8 )
	{
	    uint32_t firstword = (m_type == DataBuffer::LITTLE_ENDIAN_WORD)?
                wordFromLitEndChars(m_buffer+position) :
	        wordFromBigEndChars(m_buffer+position);
            wordIntoBit16(firstword,con,tag);
	    
	    uint32_t secondword = (m_type == DataBuffer::LITTLE_ENDIAN_WORD)?
                wordFromLitEndChars(m_buffer+position+WRDSIZE) :
	        wordFromBigEndChars(m_buffer+position+WRDSIZE);
	    wordIntoBit16(secondword,datasize,version);
	}
	
	if( tag==0xabba && con != 0xabba && datasize != 0x0) {
	   ++m_events_read;
	   	   
	   if( datasize >= m_buffer_size) 
	       throw BufferException(datasize,m_buffer_size);
	   
	   if( datasize > m_buffer_length ) {
	       //position -= read_next_data(m_end_of_current_event);
	       position -= read_next_data(position);
	   }
	   	   
	   // check consistency if space is available
	   if ( datasize <= m_buffer_length ) {	   
	       uint16_t closingcon,closingtag;
	       uint32_t lastword =  (m_type == DataBuffer::LITTLE_ENDIAN_WORD)?
                   wordFromLitEndChars(m_buffer+position+datasize-WRDSIZE) :
	           wordFromBigEndChars(m_buffer+position+datasize-WRDSIZE);
	      
               wordIntoBit16(lastword,closingtag,closingcon);
	        
	       if(tag!=closingtag || con!=closingcon) {
	           
		   if (m_events_read > m_events_to_skip) {
		       ++m_events_corrupted;
	               if(m_debug) {
	                  //do something ..... event corrupted
	               }
		       m_events_processed += 1;
		   }
	       
	       } else {

                   m_start_of_current_event = position;
	           m_end_of_current_event   = position+datasize;
	           
		   if (m_events_read > m_events_to_skip) {
		       if( m_bytes_skipped && m_debug ) {
	                   //do something ... words skipped
	               }
		       m_events_processed += 1;
	               return m_start_of_current_event;
	           } else {
	               position        += datasize-1;
		       m_buffer_length -= datasize-1;
		       if (m_buffer_length==1) {
                           position -= read_next_data(m_end_of_current_event); 
                       }
		   }
	       }
	   } else {
	       ++m_bytes_skipped;
	       if(m_bytes_skipped < m_buffer_size)
	           m_skipped[m_bytes_skipped] = m_buffer[position];
	   }
	   
	} else {
	    ++m_bytes_skipped;
	    if(m_bytes_skipped < m_buffer_size)
	        m_skipped[m_bytes_skipped] = m_buffer[position];
	}

        position +=1;
	m_buffer_length -=1;	
    }
    
    // file in eof
    return m_buffer_size;
}


bool
CalibDataLoader::next(DataBuffer& data) {
   
   uint32_t position = find_next();
   
   if(position == m_buffer_size) return false;

   uint32_t datasize = m_end_of_current_event-m_start_of_current_event;

   data = DataBuffer(m_buffer + m_start_of_current_event, datasize);

   m_tot_bytes_processed += datasize;
   m_buffer_length       -= datasize;
   
   return true; 
}

uint32_t
CalibDataLoader::read_data(char* p, uint32_t s) {
        
    memset (p,0xff,s);
    m_file->readData(p,s);
    
    while (s) {
        uint32_t* v = reinterpret_cast<uint32_t*>(p+s-4);
	if (*v != 0xffffffff) break;
	s -= 4;
    }
    
    if( s%4 && (s%4+m_buffer_length%4)%4 )  throw ReadDataException(s);
    
    return s;
}

std::ostream& operator<<
                      (std::ostream& stream, const CalibDataLoader& loader) {

    stream << "CalibDataLoader summary" << std::endl;
    stream << "=======================" << std::endl;
    stream << "  File accessed       : " << loader.filename() << std::endl;
    stream << "  Total bytes read    : " << loader.tot_bytes_read() << std::endl;
    stream << "  Total bytes skipped : " << loader.tot_bytes_read()-loader.tot_bytes_processed() << std::endl;
    stream << "  Bytes processed     : " << loader.tot_bytes_processed() << std::endl;
    stream << "  Events read         : " << loader.events_read() << std::endl;
    stream << "  Events processed    : " << loader.events_processed() << std::endl;
    stream << "  Bad events found    : " << loader.events_corrupted() << std::endl;
    return stream;
}
