#include <stdio.h>
#include <signal.h>

#include "circ/Error.h"
#include "circ/Circ.h"
#include "circ/Circservice.h"

#include "MuCalDecode/CalibDataLoader.h"
#include "MuCalDecode/DataBuffer.h"
#include "MuCalDecode/CalibEvent.h"

char *filename = (char *) NULL;
char *circname = (char *) NULL;

int cid = -1;
int circsize = -1;
int interval = 1000;
int maxnum = -1;

unsigned short circserver_port = CIRC_SERVER_DEFAULT_PORT;

bool end_loop = false;
bool dump_f = false;
bool dump_x = false;
bool circ = false;

LVL2_MUON_CALIBRATION::CalibDataLoader *loader = 0;

void test_mucal_dataloader_signal_handler (int val)
{
  std::cout << "Signal " << val << " received";
  end_loop = true;
}

int test_mucal_dataloader_help ()
{
  std::cout << "Usage: test_mucal_dataloader [options] <filename>" << std::endl
            << "\t\t\tOptions:" << std::endl
            << "\t\t\t\t[-x]\t\thex dump" << std::endl
            << "\t\t\t\t[-d]\t\tformatted dump" << std::endl
            << "\t\t\t\t[-n <num>]\t\tmax event number" << std::endl
            << "\t\t\t\t[-c circ_name circ_size]" << std::endl
            << "\t\t\t\t[-p circserver_port]" << std::endl;

  return Error_YES;
}

int test_mucal_dataloader_opt (int c, char **v)
{
  int i;

  for (i=1; i < c ; ++i)
    if (v[i][0] == '-')
    {
      switch (v[i][1])
      {
        case 'd':
          dump_f = true;
          break;
        case 'x':
          dump_x = true;
          break;
        case 'c':
	  circname = v[++i];
	  circsize = atoi (v[++i]);
	  circ = true;
	  break;
        case 'n':
          maxnum = atoi (v[++i]);
          break;
        case 'p':
	  circserver_port = static_cast <unsigned short> (atoi (v[++i]));
	  break;
        default:
          return test_mucal_dataloader_help ();
      }
    }
    else
      filename = v[i];

  if (! filename)
    return test_mucal_dataloader_help ();

  if (circ && (circsize < 0 || circname == (char *) NULL))
    return test_mucal_dataloader_help ();

  return Error_NO;
}

int test_mucal_dataloader_init ()
{
  int err = Error_NO;

  signal (SIGTERM, test_mucal_dataloader_signal_handler);
  signal (SIGINT, test_mucal_dataloader_signal_handler);

  /* Open the data file */

  std::string file_string (filename);

  try
  {
    loader = new LVL2_MUON_CALIBRATION::CalibDataLoader (file_string, true);
  }

  catch (...)
  {
    loader = 0;
    err = Error_YES;
    return err;
  }

  if (circ)
  {
    /* Ask the server to open a buffer and connect to it */

    if ((cid = CircOpenCircConnection (circserver_port, circname, circsize)) < 0)
      err = Error_YES;
  }

  return err;
}

int test_mucal_dataloader_end ()
{
  int err = Error_NO;

  std::cout << "Total events: " << loader->events_read () << std::endl;
  std::cout << "Total bytes: " << loader->tot_bytes_read () << std::endl;

  if (circ && cid >= 0)
  {
    /* Ask the server to close the buffer and close the buffer connection */

    err = CircCloseCircConnection (circserver_port, circname, cid);
  }

  if (loader)
    delete loader;

  return err;
}

unsigned int test_mucal_dataloader_read_event (DataBuffer &data)
{
  unsigned int size;

  if (loader->next (data))
    size = static_cast <unsigned int> (data.size ());
  else
    size = 0;

  return size;
}

int test_mucal_dataloader_loop ()
{
  int err = Error_NO;
  int number = 0;
  unsigned int evtsize;

  uint16_t short_size;

  char *ptr;

  DataBuffer data;

  /* Read out and process data until end of file is reached or an interrupt is received */

  while (! end_loop && number != maxnum)
  {
    if ((evtsize = test_mucal_dataloader_read_event (data)) != 0)
    {
      short_size = static_cast <uint16_t> (evtsize);

      if (dump_x)
      {
	std::cout << "Event " << number << " size " << evtsize << std::endl;
	std::cout << LVL2_MUON_CALIBRATION::hexDump (data.data (), short_size) << std::endl;
      }

      LVL2_MUON_CALIBRATION::CalibEvent event (data);

      if (dump_f)
	std::cout << event << std::endl;

      if (circ && event.isValid ())
      {
	if ((ptr = CircReserve (cid, number, evtsize)) != (char *) -1)
	{
	  memcpy (ptr, data.data (), evtsize);

	  (void) CircValidate (cid, number, ptr, evtsize);
	  sched_yield ();

	  if (interval && ((number + 1) % interval == 0))
	    printf (" Circclient> Number= %d \n", (number + 1));
	}
      }

      ++number;
    }
    else
      return err;
  }

  return err;
}

int main (int argc, char **argv)
{
  int err = 0;

  if ((err = test_mucal_dataloader_opt (argc, argv)) != Error_NO)
    return err;

  if ((err = test_mucal_dataloader_init ()) != Error_NO)
    return err;

  if ((err = test_mucal_dataloader_loop ()) != Error_NO)
    return err;

  if ((err = test_mucal_dataloader_end ()) != Error_NO)
    return err;

  return err;
}
