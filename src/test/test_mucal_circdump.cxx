#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sched.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/select.h>

#include "MuCalDecode/DataBuffer.h"
#include "MuCalDecode/CalibEvent.h"

#include "circ/Error.h"
#include "circ/Circ.h"
#include "circ/Circservice.h"

unsigned short port = CIRC_SERVER_DEFAULT_PORT;

bool end_loop = false;
bool dump_x = false;
bool dump_f = false;

/* Thread conditions */

enum Condition
{
  UNSTARTED,
  RUNNING,
  CANCEL_REQUESTED,
  CLEANING_UP,
  TERMINATED
};

/* Only one single thread is started to deal with circ readout.
 * This implies that the following variables can be global.     */

pthread_t id_;
pthread_attr_t attributes_;
pthread_mutex_t mutex_;
pthread_mutex_t request_mutex_;
pthread_mutex_t cond_mutex_;
pthread_cond_t cond_;

enum Condition condition_;

void test_mucal_circdump_thread_specific_cleanup ()
{
}

void test_mucal_circdump_thread_run ()
{
  int nc = 0, cidlen = 0;
  static int *cids = (int *) NULL;

  int i;
  
  while (true)
  {
    /* Locking at each event can severily affect performance */

    pthread_mutex_lock (&request_mutex_);

    /* Check the list of connected cids */

    nc = CircServerGetCircList (&cids, &cidlen);

    for (i = 0; i < nc; ++i)
    {
      char *ptr;
      int number, evtsize;
      uint16_t shortsize;

      if ((ptr = CircLocate (cids[i], &number, &evtsize)) != (char *) -1)
      {
	shortsize = static_cast <uint16_t> (evtsize);

	DataBuffer buf ((uint8_t *) ptr, shortsize);
	LVL2_MUON_CALIBRATION::CalibEvent event (buf);

        if (event.isValid ())
        {
          std::cout << "Event size = "
                    << evtsize << std::endl;

	  if (dump_x)
	    std::cout << LVL2_MUON_CALIBRATION::hexDump (buf.data (), shortsize) << std::endl;

	  if (dump_f)
	    std::cout << event << std::endl;
        }
        else
          std::cerr << "Event is invalid" << std::endl;

	(void) CircRelease (cids[i]);
      }
    }

    pthread_mutex_unlock (&request_mutex_);

    sched_yield ();
    pthread_testcancel ();
  }

  if (cids)
    free (cids);
}

void test_mucal_circdump_thread_init ()
{
  pthread_mutex_init (&mutex_, 0);
  pthread_mutex_init (&cond_mutex_, 0);
  pthread_mutex_init (&request_mutex_, 0);
  pthread_cond_init (&cond_, 0);
  pthread_attr_init (&attributes_);

  /* Set thread attributes */

  pthread_attr_setdetachstate (&attributes_, PTHREAD_CREATE_DETACHED);
}

int test_mucal_circdump_thread_wait_for_condition (enum Condition condition, int timeout)
{
  int code = 0;
  struct timespec timeLimit = {0, 0};
  timeLimit.tv_sec = time (0) + timeout;

  pthread_mutex_lock (&cond_mutex_);

  while (condition_ < condition)
  {
    if (pthread_cond_timedwait (&cond_, &cond_mutex_, &timeLimit) == ETIMEDOUT)
    {
      code = -1;
      break;
    }
  }

  pthread_mutex_unlock (&cond_mutex_);

  return code;
}

void test_mucal_circdump_thread_set_condition (enum Condition condition) 
{
  pthread_mutex_lock (&cond_mutex_);
  condition_ = condition;
  pthread_cond_broadcast (&cond_);
  pthread_mutex_unlock (&cond_mutex_);
}

void test_mucal_circdump_thread_cleanup (void *)
{
  pthread_mutex_lock (&mutex_);
  test_mucal_circdump_thread_set_condition (CLEANING_UP);
  test_mucal_circdump_thread_specific_cleanup ();
  test_mucal_circdump_thread_set_condition (TERMINATED);
  pthread_mutex_unlock (&mutex_);
}

void *test_mucal_circdump_thread_entry_point (void *)
{
  /* Update cleanup stack */

  pthread_cleanup_push (test_mucal_circdump_thread_cleanup, 0);

  pthread_setcanceltype (PTHREAD_CANCEL_DEFERRED, 0);
  pthread_setcancelstate (PTHREAD_CANCEL_ENABLE, 0);

  /* Run the user code */

  test_mucal_circdump_thread_run ();

  pthread_cleanup_pop (1);

  return (0);
}

int test_mucal_circdump_thread_start_execution () 
{
  int rc = 1;

  pthread_mutex_lock (&mutex_);

  if (condition_ == RUNNING)
  {
    rc = 1;
  }
  else if ((rc = pthread_create (&id_, &attributes_,
				 test_mucal_circdump_thread_entry_point, 0)) == 0)
  {
    test_mucal_circdump_thread_set_condition (RUNNING);
  }

  pthread_mutex_unlock (&mutex_);

  return rc;
}

void test_mucal_circdump_thread_stop_execution () 
{
  pthread_mutex_lock (&mutex_);

  if (condition_ == RUNNING)
  {
    test_mucal_circdump_thread_set_condition (CANCEL_REQUESTED);

    if (id_ == pthread_self ())
    {
      pthread_exit (0);
    }
    else
    {
      pthread_cancel (id_);
    }
  }

  pthread_mutex_unlock (&mutex_);
}

void test_mucal_circdump_thread_end ()
{
  pthread_mutex_destroy (&mutex_);
  pthread_mutex_destroy (&cond_mutex_);
  pthread_mutex_destroy (&request_mutex_);
  pthread_cond_destroy (&cond_);
}

void test_mucal_circdump_signal_handler (int val)
{
  printf ("Signal %d received\n", val);
  end_loop = true;
}

int test_mucal_circdump_help ()
{
  printf ("Usage: Circserver [-x] [-d] [-p port]\n");
  return -1;
}

int test_mucal_circdump_opt (int c, char **v)
{
  int i;

  for (i=1; i < c ; ++i)
    if (v[i][0] == '-')
    {
      switch (v[i][1])
      {
        case 'p':
          port = (unsigned short) atoi (v[++i]);
          break;
        case 'x':
	  dump_x = true;
	  break;
        case 'd':
	  dump_f = true;
	  break;
        default:
          return test_mucal_circdump_help ();
      }
    }

  return 0;
}

int test_mucal_circdump_init ()
{
  /* Register signal handlers */

  signal (SIGTERM, test_mucal_circdump_signal_handler);
  signal (SIGINT, test_mucal_circdump_signal_handler);

  /* Initialize the reading thread */

  test_mucal_circdump_thread_init ();

  /* Initialize the server */

  return (CircServerInit (port));
}

int test_mucal_circdump_loop ()
{
  int err = 0;
  time_t t, t0;
  static double long_interval = 10.;
  static struct timeval tout = {0, 100000};

  test_mucal_circdump_thread_start_execution ();

  time (&t0);

  while (! end_loop)
  {
    struct timeval tout_v;

    if (CircServerCheckRequest ())
    {
      pthread_mutex_lock (&request_mutex_);
      CircServerServeRequest ();
      pthread_mutex_unlock (&request_mutex_);
    }

    time (&t);

    if (difftime (t, t0) > long_interval)
    {
      pthread_mutex_lock (&request_mutex_);
      CircServerGarbageCollector ();
      pthread_mutex_unlock (&request_mutex_);

      time (&t0);
    }

    memcpy (&tout_v, &tout, sizeof (struct timeval));
    select (0, 0, 0, 0, &tout_v);
  }

  test_mucal_circdump_thread_stop_execution ();
  test_mucal_circdump_thread_wait_for_condition (TERMINATED, 5);

  return err;
}

int test_mucal_circdump_end ()
{
  int err = 0;

  /* End the server */

  err = CircServerEnd ();

  /* Stop the reading thread */

  test_mucal_circdump_thread_end ();

  return err;
}

int main (int argc, char **argv)
{
  int err = 0;

  if ((err = test_mucal_circdump_opt (argc, argv)))
    return err;

  if ((err = test_mucal_circdump_init ()))
    return err;

  if ((err = test_mucal_circdump_loop ()))
    return err;

  if ((err = test_mucal_circdump_end ()))
    return err;

  return err;
}
