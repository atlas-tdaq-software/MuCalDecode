#include "MuCalDecode/DataBuffer.h"

DataBuffer::DataBuffer(DataBuffer::WordType type) : m_type(type),
                                                    m_size(0),
						    m_start(0),
						    m_data(0),
						    m_void(0x0),
						    m_overflow(false)
{}

DataBuffer::DataBuffer(uint8_t* data, uint16_t size, 
                       DataBuffer::WordType type) : m_type(type),
                                                    m_size(size),
						    m_start(0),
						    m_data(0),
						    m_void(0x0),
						    m_overflow(false)
{
    //m_data = new uint8_t[m_size];
    m_data = data;
    //memset(m_data,0,m_size);
    //memcpy(m_data,data,m_size);
}

/*
DataBuffer::DataBuffer(const DataBuffer& buffer)
{
    m_type = buffer.type();
    m_size = buffer.size();
    m_start = buffer.start();
    
    m_data = new uint8_t[m_size];
    //memset(m_data,0,m_size);
    memcpy(m_data,buffer.data(),m_size);

    m_void = 0x0;
    
    m_overflow = buffer.isOverflow();
}*/

DataBuffer::DataBuffer(const DataBuffer& buffer)
{
    m_type = buffer.type();
    m_size = buffer.size();
    m_start = buffer.start();
    
    m_data = buffer.data();
    m_void = 0x0;
    
    m_overflow = buffer.isOverflow();
}

DataBuffer::~DataBuffer()  
{
    //if (m_data) delete m_data;
}

/*
DataBuffer
DataBuffer::operator=(const DataBuffer& buffer)
{
    m_type = buffer.type();
    m_size = buffer.size();
    m_start = buffer.start();
    m_posit = buffer.posit();

    if(m_data) delete m_data;
    m_data = new uint8_t[m_size];
    //memset(m_data,0,m_size);
    memcpy(m_data,buffer.data(),m_size);
    
    m_void = 0x0;
    
    m_overflow = buffer.isOverflow();
    return *this;
}*/


DataBuffer
DataBuffer::operator=(const DataBuffer& buffer)
{
    m_type = buffer.type();
    m_size = buffer.size();
    m_start = buffer.start();
    m_posit = buffer.posit();

    m_data = buffer.data();
    
    m_void = 0x0;
    
    m_overflow = buffer.isOverflow();
    return *this;
}



DataBuffer
DataBuffer::operator+(uint16_t pos)
{
    uint16_t position = m_start+pos;
    if( position < m_size )
    {
        uint8_t* data = this->ptr(pos);
	uint16_t size = m_size - pos;
	return DataBuffer(data,size);
    }
    return DataBuffer();
}

uint8_t*
DataBuffer::ptr(uint16_t pos)
{
    uint16_t position = m_start+pos;
    if( position < m_size )
    {
        return  m_data+position;
    } else 
    {
        m_overflow = true;
        return 0;
    }
}

std::ostream  &operator<< (std::ostream& stream, DataBuffer& buffer)
{
    stream << "DataBuffer of size " << buffer.size() 
           << " contains the following words:" << std::endl;

    for (int i=0; i<buffer.size();i=i+4)
    {
        LVL2_MUON_CALIBRATION::printWord(stream,buffer.value(i));
	stream << std::endl;
    }
    
    return stream;
}
