#ifndef CALIBDATALOADER_H
#define CALIBDATALOADER_H

#include "MuCalDecode/DataBuffer.h"
#include <string>
#include <iostream>
#include <exception>
#include <sstream>

#include "EventStorage/loadfRead.h"
#include "EventStorage/DataReader.h"

#define DEFAULT_BUFFER_SIZE 500000

namespace LVL2_MUON_CALIBRATION {


class BufferException : public std::exception { 

  public:
  
  BufferException(uint32_t datasize, uint32_t buffersize) : 
              m_datasize(datasize),
              m_buffersize(buffersize) {}
  
  virtual const char* what() const throw()
  {
    std::ostringstream msg;
    
    msg << "CalibDataLoader: buffer length exception!" << std::endl
        << " Read event of size " << m_datasize 
	<< " bytes while internal buffer is of size " << m_buffersize
	<< " bytes" << std::endl 
	<< " Increase MAX_BUFFER_SIZE and recompile!" << std::endl;
	
    return msg.str().c_str();
  }

  private:
  uint32_t m_datasize;
  uint32_t m_buffersize;

};

class ReadDataException : public std::exception { 

  public:
  
  ReadDataException(uint32_t datasize) : m_datasize(datasize) {}
  
  virtual const char* what() const throw()
  {
    std::ostringstream msg;
    
    msg << "CalibDataLoader::read_data() .. buffer read exception!" << std::endl
        << " Read " << m_datasize 
	<< " bytes which are not multiple of 32-bit words" << std::endl 
	<< " Still 16-bit mismatch in the file? " << std::endl;
	
    return msg.str().c_str();
  }

  private:
  uint32_t m_datasize;
};




class CalibDataLoader {

    public:
    CalibDataLoader();
    
    CalibDataLoader(std::string filename, 
                    bool debug = false,
		    uint32_t events = 0xffffffff,
		    uint32_t toSkip = 0x0,
		    uint32_t size = DEFAULT_BUFFER_SIZE, 
		    DataBuffer::WordType type = DataBuffer::LITTLE_ENDIAN_WORD);
    
    //CalibDataLoader(const CalibDataLoader&) {}
    
    ~CalibDataLoader();
 
    bool next(DataBuffer&);
    
    std::string filename(void) const;
    uint32_t    events_read(void) const;
    uint32_t    events_processed(void) const; 
    uint32_t    events_corrupted(void) const;
    uint32_t    tot_bytes_read(void) const;
    uint32_t    tot_bytes_processed(void) const;

    private:
    fRead*               m_file;
    //DataReader*          m_file;
    std::string          m_filename;
    bool                 m_debug;
    DataBuffer::WordType m_type;

    uint32_t             m_buffer_length;
    uint32_t             m_start_of_current_event;
    uint32_t             m_end_of_current_event;
    
    uint32_t             m_tot_bytes_read;
    uint32_t             m_tot_bytes_processed;

    uint32_t             m_max_event_to_process;
    uint32_t             m_events_to_skip;
    uint32_t             m_events_read;
    uint32_t             m_events_processed;
    uint32_t             m_events_corrupted;
    uint32_t             m_bytes_skipped;
    
    bool                 m_bad_event_flag;
    
    uint32_t             m_buffer_size;
    uint8_t*             m_buffer; 
    uint8_t*             m_skipped;
    
    
    
    
    fRead*    connect_file(std::string);
    uint32_t       read_next_data(uint32_t);
    uint32_t       find_next(void);
    uint32_t       read_data(char*,uint32_t);
};

inline std::string CalibDataLoader::filename() const         {return m_filename;}
inline uint32_t    CalibDataLoader::events_read() const      {return m_events_read;}
inline uint32_t    CalibDataLoader::events_corrupted() const {return m_events_corrupted;}
inline uint32_t    CalibDataLoader::events_processed() const {return m_events_processed;}
inline uint32_t    CalibDataLoader::tot_bytes_read() const   {return m_tot_bytes_read;}
inline uint32_t    CalibDataLoader::tot_bytes_processed() const{return m_tot_bytes_processed;}
}

std::ostream& operator<<(std::ostream&, const LVL2_MUON_CALIBRATION::CalibDataLoader&);
#endif
