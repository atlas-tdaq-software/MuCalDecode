#ifndef DATABUFFER_H
#define DATABUFFER_H

#include <iostream>
#include "MuCalDecode/CalibUti.h"


class DataBuffer {

    public:
    typedef enum {LITTLE_ENDIAN_WORD, BIG_ENDIAN_WORD} WordType;    
    
    
    DataBuffer(WordType=LITTLE_ENDIAN_WORD);
    DataBuffer(uint8_t* data, uint16_t size,WordType=LITTLE_ENDIAN_WORD);
    DataBuffer(const DataBuffer&);
    ~DataBuffer();
    
    void setmarker(uint16_t);
    void addmarker(uint16_t);
    void rewind(void);
    
    
    DataBuffer operator=(const DataBuffer&);
    DataBuffer operator+(uint16_t);
    
    uint32_t value(uint16_t pos=0);
    uint8_t* ptr(uint16_t pos=0);
    
    
    WordType type(void) const;
    uint16_t size(void) const;
    uint16_t start(void) const;
    uint16_t posit(void) const;
    uint8_t* data(void) const;
    
    bool isOverflow(void) const;
    
    
    private:
    WordType m_type;
    uint16_t m_size;
    uint16_t m_start;
    uint16_t m_posit;
    uint8_t* m_data;
    
    uint32_t m_void;
    
    bool m_overflow;    
};

inline uint16_t DataBuffer::size() const {return m_size;}

inline DataBuffer::WordType DataBuffer::type() const {return m_type;}
inline uint16_t DataBuffer::start() const {return m_start;}
inline uint16_t DataBuffer::posit() const {return m_posit;}
inline uint8_t* DataBuffer::data() const {return m_data;}

inline bool DataBuffer::isOverflow() const {return m_overflow;}

inline uint32_t DataBuffer::value(uint16_t pos) {
    uint16_t position = m_start+pos;
    if( position < m_size )
    {
        return  (m_type == LITTLE_ENDIAN_WORD)?
                  LVL2_MUON_CALIBRATION::wordFromLitEndChars(m_data+position) :
	          LVL2_MUON_CALIBRATION::wordFromBigEndChars(m_data+position);
    } else 
    {
        m_overflow = true;
        return m_void;
    }
}


inline void DataBuffer::setmarker(uint16_t pos) {
    uint16_t position = pos;
    if( position < m_size )
    {
        m_start = position;
    } else m_start = m_size-1;
    m_posit = m_start;
}

inline void DataBuffer::addmarker(uint16_t pos) {
    uint16_t position = m_start + pos;
    if( position < m_size )
    {
        m_start = position;
    } else m_start = m_size-1;
}

inline void DataBuffer::rewind() {
    m_start = m_posit;
}

std::ostream  &operator<< (std::ostream& , const DataBuffer&);
#endif
