#ifndef _CALIBCHECK_H_
#define _CALIBCHECK_H_

namespace LVL2_MUON_CALIBRATION
 {
  int CalibCheckA ( void * ptr, uint32_t size=0);
  int CalibCheckB ( void * ptr, uint32_t size=0);
  int CalibCheck ( void * ptr, uint32_t size=0);
 }   // end of namespace LVL2_MUON_CALIBRATION

#endif
